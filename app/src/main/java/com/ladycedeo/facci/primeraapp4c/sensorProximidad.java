package com.ladycedeo.facci.primeraapp4c;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class sensorProximidad extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;// object class
    Sensor sensorProximidad;
    TextView textViewProximidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_proximidad);

        textViewProximidad = (TextView) findViewById(R.id.textViewProximidad);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE); //para obtener un servicio del sistema (casting, es para que una view busque una id con algun identificador)
        sensorProximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    protected void onResume() {//polimorfismo, es usar un mismo metodo o procedimiento pero en distintos elementos.
        super.onResume();
        //aqui debemos registrar el listener
        sensorManager.registerListener(this, sensorProximidad, sensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        textViewProximidad.setText(Float.toString( event.values[0]));
        if (event.values[0] < sensorProximidad.getMaximumRange()){
            getWindow().getDecorView().setBackgroundColor(Color.GREEN);
            //event.values[0]
            //event.values[1]
            //event.values[2]

        }
        else{
            getWindow().getDecorView().setBackgroundColor(Color.RED);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
