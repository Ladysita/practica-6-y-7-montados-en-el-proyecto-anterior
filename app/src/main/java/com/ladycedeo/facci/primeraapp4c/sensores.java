package com.ladycedeo.facci.primeraapp4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class sensores extends AppCompatActivity {
    Button acelerometro , proximidad,luz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);

        acelerometro = (Button)findViewById(R.id.acelerometro);
        proximidad = (Button)findViewById(R.id.proximidad);

        acelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(sensores.this,sensorAcelerometro.class);
                startActivity(intent);
            }
        });


        proximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(sensores.this,sensorProximidad.class);
                startActivity(intent);
            }
        });
    }
}
