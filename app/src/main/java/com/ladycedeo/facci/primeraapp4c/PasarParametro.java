package com.ladycedeo.facci.primeraapp4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasarParametro extends AppCompatActivity {

    EditText cajaDatos;
    Button enviar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        cajaDatos = (EditText)findViewById(R.id.txtParametro);
        enviar = (Button)findViewById(R.id.EnviarParametro);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PasarParametro.this,RecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString ("dato", cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);


            }

        });

    }
}
